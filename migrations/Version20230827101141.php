<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230827101141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lesson CHANGE date date DATE NOT NULL');
        $this->addSql('ALTER TABLE masterclass ADD lesson_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE masterclass ADD CONSTRAINT FK_9BDB44EDCDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id)');
        $this->addSql('CREATE INDEX IDX_9BDB44EDCDF80196 ON masterclass (lesson_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lesson CHANGE date date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE masterclass DROP FOREIGN KEY FK_9BDB44EDCDF80196');
        $this->addSql('DROP INDEX IDX_9BDB44EDCDF80196 ON masterclass');
        $this->addSql('ALTER TABLE masterclass DROP lesson_id');
    }
}
