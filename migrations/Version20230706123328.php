<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230706123328 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE lesson (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_F87474F3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE masterclass (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, video VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, school VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opinion (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, lesson_id INT NOT NULL, note VARCHAR(255) NOT NULL, grade INT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_AB02B027A76ED395 (user_id), INDEX IDX_AB02B027CDF80196 (lesson_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, lesson_id INT NOT NULL, content VARCHAR(255) NOT NULL, INDEX IDX_B6F7494ECDF80196 (lesson_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ref_category (id INT AUTO_INCREMENT NOT NULL, note VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ref_category_masterclass (ref_category_id INT NOT NULL, masterclass_id INT NOT NULL, INDEX IDX_A13E858EE84F656E (ref_category_id), INDEX IDX_A13E858E426F0705 (masterclass_id), PRIMARY KEY(ref_category_id, masterclass_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ref_composer (id INT AUTO_INCREMENT NOT NULL, note VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ref_composer_masterclass (ref_composer_id INT NOT NULL, masterclass_id INT NOT NULL, INDEX IDX_962E22F78084DEAC (ref_composer_id), INDEX IDX_962E22F7426F0705 (masterclass_id), PRIMARY KEY(ref_composer_id, masterclass_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ref_format (id INT AUTO_INCREMENT NOT NULL, note VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ref_format_masterclass (ref_format_id INT NOT NULL, masterclass_id INT NOT NULL, INDEX IDX_5AADB9A7A573F740 (ref_format_id), INDEX IDX_5AADB9A7426F0705 (masterclass_id), PRIMARY KEY(ref_format_id, masterclass_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE response (id INT AUTO_INCREMENT NOT NULL, question_id INT NOT NULL, content VARCHAR(255) NOT NULL, INDEX IDX_3E7B0BFB1E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT FK_AB02B027A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT FK_AB02B027CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494ECDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id)');
        $this->addSql('ALTER TABLE ref_category_masterclass ADD CONSTRAINT FK_A13E858EE84F656E FOREIGN KEY (ref_category_id) REFERENCES ref_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ref_category_masterclass ADD CONSTRAINT FK_A13E858E426F0705 FOREIGN KEY (masterclass_id) REFERENCES masterclass (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ref_composer_masterclass ADD CONSTRAINT FK_962E22F78084DEAC FOREIGN KEY (ref_composer_id) REFERENCES ref_composer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ref_composer_masterclass ADD CONSTRAINT FK_962E22F7426F0705 FOREIGN KEY (masterclass_id) REFERENCES masterclass (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ref_format_masterclass ADD CONSTRAINT FK_5AADB9A7A573F740 FOREIGN KEY (ref_format_id) REFERENCES ref_format (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ref_format_masterclass ADD CONSTRAINT FK_5AADB9A7426F0705 FOREIGN KEY (masterclass_id) REFERENCES masterclass (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE response ADD CONSTRAINT FK_3E7B0BFB1E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lesson DROP FOREIGN KEY FK_F87474F3A76ED395');
        $this->addSql('ALTER TABLE opinion DROP FOREIGN KEY FK_AB02B027A76ED395');
        $this->addSql('ALTER TABLE opinion DROP FOREIGN KEY FK_AB02B027CDF80196');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494ECDF80196');
        $this->addSql('ALTER TABLE ref_category_masterclass DROP FOREIGN KEY FK_A13E858EE84F656E');
        $this->addSql('ALTER TABLE ref_category_masterclass DROP FOREIGN KEY FK_A13E858E426F0705');
        $this->addSql('ALTER TABLE ref_composer_masterclass DROP FOREIGN KEY FK_962E22F78084DEAC');
        $this->addSql('ALTER TABLE ref_composer_masterclass DROP FOREIGN KEY FK_962E22F7426F0705');
        $this->addSql('ALTER TABLE ref_format_masterclass DROP FOREIGN KEY FK_5AADB9A7A573F740');
        $this->addSql('ALTER TABLE ref_format_masterclass DROP FOREIGN KEY FK_5AADB9A7426F0705');
        $this->addSql('ALTER TABLE response DROP FOREIGN KEY FK_3E7B0BFB1E27F6BF');
        $this->addSql('DROP TABLE lesson');
        $this->addSql('DROP TABLE masterclass');
        $this->addSql('DROP TABLE opinion');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE ref_category');
        $this->addSql('DROP TABLE ref_category_masterclass');
        $this->addSql('DROP TABLE ref_composer');
        $this->addSql('DROP TABLE ref_composer_masterclass');
        $this->addSql('DROP TABLE ref_format');
        $this->addSql('DROP TABLE ref_format_masterclass');
        $this->addSql('DROP TABLE response');
        $this->addSql('DROP TABLE user');
    }
}
