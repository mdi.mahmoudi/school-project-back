<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230808170339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_lessons (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, progression INT NOT NULL, INDEX IDX_674F06D3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_lessons_lesson (user_lessons_id INT NOT NULL, lesson_id INT NOT NULL, INDEX IDX_3167699759B7B5B9 (user_lessons_id), INDEX IDX_31676997CDF80196 (lesson_id), PRIMARY KEY(user_lessons_id, lesson_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_lessons ADD CONSTRAINT FK_674F06D3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_lessons_lesson ADD CONSTRAINT FK_3167699759B7B5B9 FOREIGN KEY (user_lessons_id) REFERENCES user_lessons (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_lessons_lesson ADD CONSTRAINT FK_31676997CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lesson ADD presentation LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE user ADD presentation LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_lessons DROP FOREIGN KEY FK_674F06D3A76ED395');
        $this->addSql('ALTER TABLE user_lessons_lesson DROP FOREIGN KEY FK_3167699759B7B5B9');
        $this->addSql('ALTER TABLE user_lessons_lesson DROP FOREIGN KEY FK_31676997CDF80196');
        $this->addSql('DROP TABLE user_lessons');
        $this->addSql('DROP TABLE user_lessons_lesson');
        $this->addSql('ALTER TABLE user DROP presentation');
        $this->addSql('ALTER TABLE lesson DROP presentation');
    }
}
