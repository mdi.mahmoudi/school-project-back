<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230905170048 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_lessons ADD masterclass_id INT DEFAULT NULL, ADD state VARCHAR(255) NOT NULL, DROP progression');
        $this->addSql('ALTER TABLE user_lessons ADD CONSTRAINT FK_674F06D3426F0705 FOREIGN KEY (masterclass_id) REFERENCES masterclass (id)');
        $this->addSql('CREATE INDEX IDX_674F06D3426F0705 ON user_lessons (masterclass_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_lessons DROP FOREIGN KEY FK_674F06D3426F0705');
        $this->addSql('DROP INDEX IDX_674F06D3426F0705 ON user_lessons');
        $this->addSql('ALTER TABLE user_lessons ADD progression INT NOT NULL, DROP masterclass_id, DROP state');
    }
}
