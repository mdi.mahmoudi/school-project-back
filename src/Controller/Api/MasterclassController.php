<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\LessonRepository;
use App\Repository\MasterclassRepository;
use App\Repository\UserLessonsRepository;
use App\Service\ModuleManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\SerializerInterface;

class MasterclassController extends AbstractController
{
    #[Route('/api/masterclass/{id}', name: 'api_masterclass')]
    public function index(
        string $id,
        MasterclassRepository $masterclassRepository,
        SerializerInterface $serializer,
        Request $request,
        #[CurrentUser] ?User $user,
        UserLessonsRepository $userLessonsRepository,
        LessonRepository $lessonRepository,
    ) {
        $data = json_decode($request->getContent(), true);
        $lessonId = $data['lesson'];
        $userLessons = $userLessonsRepository->getUserLessons($user->getId(), $lessonId);

        $masterclass = $masterclassRepository->find($id);

        $data = $serializer->serialize([$masterclass, $userLessons], 'json', [
            'groups' => 'masterclass',
        ]);

        return new JsonResponse($data, 200, [], true);
    }

    #[Route('/api/validate/module', name: 'api_validate_module')]
    public function validateModule(
        Request $request,
        #[CurrentUser] ?User $user,
        ModuleManager $moduleManager,
    ) {
        $data = json_decode($request->getContent(), true);
        $masterclassId = $data['masterclass'];
        $moduleManager->changeModuleState($masterclassId, $user->getId());

        return new JsonResponse(['Module validate successfully']);
    }
}
