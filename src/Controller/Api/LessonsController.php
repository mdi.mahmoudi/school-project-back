<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\LessonRepository;
use App\Service\LessonManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\SerializerInterface;

class LessonsController extends AbstractController
{
    #[Route('/api/lessons', name: 'api_lessons')]
    public function index(LessonRepository $lessonRepository, SerializerInterface $serializer): JsonResponse
    {
        $lessons = $lessonRepository->getSortedLessons();

        if (!$lessons) {
            throw new NotFoundHttpException('Any lessons found');
        }

        // Serialize --> convert object to specific data format like xml or json
        $data = $serializer->serialize($lessons, 'json', [
            'groups' => 'lessons',
        ]);

        return new JsonResponse($data, 200, [], true);
    }

    #[Route('/api/landing/lesson/{id}', name: 'api_lesson')]
    public function getLesson(
        int $id,
        LessonRepository $lessonRepository,
        SerializerInterface $serializer,
        #[CurrentUser] ?User $user,
        LessonManager $lessonManager,
    ): JsonResponse {
        $lesson = $lessonRepository->find($id);

        $isAlreadyRegistered = $user ? $lessonManager->checkIfUserIsRegisteredToLesson($user->getId(), $id) : [];

        $userLessons = $isAlreadyRegistered ? $lessonManager->getUserLessons($user->getId(), $id) : [];

        $data = $serializer->serialize([$lesson, $isAlreadyRegistered, $userLessons], 'json', [
            'groups' => 'lesson',
        ]);

        return new JsonResponse($data, 200, [], true);
    }

    #[Route('api/add/lesson', name: 'api_addLesson')]
    public function addLesson(
        Request $request,
        #[CurrentUser] ?User $user,
        LessonManager $lessonManager
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);
        if (!$user) {
            return $this->json([
                'message' => 'Missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }
        $lessonManager->createLesson($user, $data['lesson'], $data['masterclass'], $data['practiceTest']);

        return new JsonResponse('Creation Successfully');
    }
}
