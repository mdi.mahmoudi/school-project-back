<?php

namespace App\Controller\Api;

use App\Entity\Lesson;
use App\Entity\Masterclass;
use App\Entity\User;
use App\Entity\UserLessons;
use App\Service\LessonManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class UserController extends AbstractController
{
    #[Route('api/inscription', name: 'api_lesson_inscription', methods: ['POST'])]
    public function index(
        Request $request,
        #[CurrentUser] ?User $user,
        EntityManagerInterface $entityManager,
        LessonManager $lessonManager,
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);

        if (!$user) {
            return $this->json([
                'message' => 'Missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        if ($lessonManager->checkIfUserIsRegisteredToLesson($user->getId(), $data['lesson'])) {
            return new JsonResponse(['Already registered']);
        }

        $nbMasterclass = count($data['masterclass']);

        for ($i = 0; $i < $nbMasterclass; ++$i) {
            $userLesson = new UserLessons();
            $masterclass = $data['masterclass'][$i]['id'];
            $userLesson->addLesson($entityManager->getReference(Lesson::class, $data['lesson']));
            $userLesson->setUser($user);
            $userLesson->setState('');
            $userLesson->setMasterclass($entityManager->getReference(Masterclass::class, $masterclass));
            $entityManager->persist($userLesson);
        }

        $entityManager->flush();

        return new JsonResponse(['Success']);
    }
}
