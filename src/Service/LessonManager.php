<?php

namespace App\Service;

use App\Entity\Lesson;
use App\Entity\Masterclass;
use App\Entity\PracticeTest;
use App\Entity\User;
use App\Repository\UserLessonsRepository;
use Doctrine\ORM\EntityManagerInterface;

class LessonManager
{
    private UserLessonsRepository $userLessonsRepository;

    private EntityManagerInterface $entityManager;

    public function __construct(UserLessonsRepository $userLessonsRepository, EntityManagerInterface $entityManager)
    {
        $this->userLessonsRepository = $userLessonsRepository;
        $this->entityManager = $entityManager;
    }

    public function checkIfUserIsRegisteredToLesson(int $idUser, int $idLesson): bool
    {
        $isRegistered = $this->userLessonsRepository->getUserLessons($idUser, $idLesson);

        return count($isRegistered) >= 1;
    }

    public function getUserLessons(int $idUser, int $idLesson): array
    {
        return $this->userLessonsRepository->getUserLessons($idUser, $idLesson);
    }

    public function createLesson(
        User $user,
        array $lesson,
        array $masterclass,
        array $practiceTest,
    ): void {
        $newLesson = new Lesson();
        $currentDateTime = date('d-m-y');
        $duration = intval($lesson[0]['duration']);
        $newLesson->setUser($user);
        $newLesson->setDescription($lesson[0]['description']);
        $newLesson->setDifficulty($lesson[0]['difficulty']);
        $newLesson->setInstrument($lesson[0]['instrument']);
        $newLesson->setDuration($duration);
        $newLesson->setTitle($lesson[0]['title']);
        $newLesson->setPresentation($lesson[0]['presentation']);
        $newLesson->setDate($currentDateTime);
        $newLesson->setNote(0);

        if (0 != count($masterclass)) {
            $this->createMasterclass($masterclass, $practiceTest, $newLesson);
        }
        $this->entityManager->persist($newLesson);
        $this->entityManager->flush();
    }

    private function createMasterclass(
        array $masterclasses,
        array $practiceTests,
        Lesson $lesson,
    ): void {
        foreach ($masterclasses as $masterclass) {
            $newMasterclass = new Masterclass();
            $newMasterclass->setLesson($lesson);
            $newMasterclass->setTitle($masterclass['title']);
            $newMasterclass->setVideo($masterclass['video']);
            $newMasterclass->setImage($masterclass['partition']);
            if (0 != count($practiceTests)) {
                foreach ($practiceTests as $practiceTest) {
                    if ($practiceTest['masterclass'] === $masterclass['count']) {
                        $newPracticeTest = new PracticeTest();
                        $newPracticeTest->setQuestion($practiceTest['question']);
                        $newPracticeTest->setAnswers([
                            $practiceTest['firstAnswer'],
                            $practiceTest['secondAnswer'],
                            $practiceTest['thirdAnswer'],
                            $practiceTest['fourthAnswer'],
                        ]);
                        $newPracticeTest->setRightAnswer($practiceTest['rightAnswer']);
                        $newPracticeTest->setMasterclass($newMasterclass);
                        $this->entityManager->persist($newPracticeTest);
                    }
                }
            }

            $this->entityManager->persist($newMasterclass);
        }

        if (0 === count($practiceTests)) {
            return;
        }
    }
}
