<?php

namespace App\Service;

use App\Repository\UserLessonsRepository;
use Doctrine\ORM\EntityManagerInterface;

class ModuleManager
{
    private UserLessonsRepository $userLessonsRepository;

    private EntityManagerInterface $entityManager;

    public function __construct(
        UserLessonsRepository $userLessonsRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->userLessonsRepository = $userLessonsRepository;
        $this->entityManager = $entityManager;
    }

    public function changeModuleState(int $idMasterclass, int $idUser): void
    {
        $userLessonsId = $this->userLessonsRepository->getUserLessonsId($idUser, $idMasterclass)[0]['id'];

        $userLessons = $this->userLessonsRepository->find($userLessonsId);
        $userLessons->setState('finish');
        $this->entityManager->persist($userLessons);
        $this->entityManager->flush();
    }
}
