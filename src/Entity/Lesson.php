<?php

namespace App\Entity;

use App\Repository\LessonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LessonRepository::class)]
class Lesson
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['lessons', 'lesson'])]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['lessons', 'lesson'])]
    private ?string $description = null;

    #[ORM\ManyToOne(inversedBy: 'lessons')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['lessons', 'lesson'])]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'lesson', targetEntity: Opinion::class)]
    #[Groups(['lessons', 'lesson'])]
    private Collection $opinions;

    #[ORM\OneToMany(mappedBy: 'lesson', targetEntity: Question::class)]
    #[Groups(['lessons', 'lesson'])]
    private Collection $questions;

    #[ORM\Column(length: 255)]
    #[Groups(['lessons', 'lesson'])]
    private ?string $title;

    #[ORM\Column(length: 255)]
    #[Groups(['lessons', 'lesson'])]
    private ?string $difficulty;

    #[ORM\Column]
    #[Groups(['lessons', 'lesson'])]
    private ?int $duration;

    #[ORM\Column(length: 255)]
    #[Groups(['lessons', 'lesson'])]
    private ?string $date = null;

    #[ORM\Column]
    #[Groups(['lessons', 'lesson'])]
    private ?int $note = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['lesson'])]
    private ?string $presentation = null;

    #[ORM\OneToMany(mappedBy: 'lesson', targetEntity: Masterclass::class)]
    #[Groups(['lesson'])]
    private Collection $masterclasses;

    #[ORM\Column(length: 255)]
    #[Groups(['lessons', 'lesson'])]
    private ?string $instrument = null;

    public function __construct()
    {
        $this->opinions = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->masterclasses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Opinion>
     */
    public function getOpinions(): Collection
    {
        return $this->opinions;
    }

    public function addOpinion(Opinion $opinion): static
    {
        if (!$this->opinions->contains($opinion)) {
            $this->opinions->add($opinion);
            $opinion->setLesson($this);
        }

        return $this;
    }

    public function removeOpinion(Opinion $opinion): static
    {
        if ($this->opinions->removeElement($opinion)) {
            // set the owning side to null (unless already changed)
            if ($opinion->getLesson() === $this) {
                $opinion->setLesson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): static
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setLesson($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): static
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getLesson() === $this) {
                $question->setLesson(null);
            }
        }

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getDifficulty(): ?string
    {
        return $this->difficulty;
    }

    public function setDifficulty(?string $difficulty): void
    {
        $this->difficulty = $difficulty;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): void
    {
        $this->duration = $duration;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): static
    {
        $this->note = $note;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(string $presentation): static
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * @return Collection<int, Masterclass>
     */
    public function getMasterclasses(): Collection
    {
        return $this->masterclasses;
    }

    public function addMasterclass(Masterclass $masterclass): static
    {
        if (!$this->masterclasses->contains($masterclass)) {
            $this->masterclasses->add($masterclass);
            $masterclass->setLesson($this);
        }

        return $this;
    }

    public function removeMasterclass(Masterclass $masterclass): static
    {
        if ($this->masterclasses->removeElement($masterclass)) {
            // set the owning side to null (unless already changed)
            if ($masterclass->getLesson() === $this) {
                $masterclass->setLesson(null);
            }
        }

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(string $instrument): static
    {
        $this->instrument = $instrument;

        return $this;
    }
}
