<?php

namespace App\Entity;

use App\Repository\RefFormatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RefFormatRepository::class)]
class RefFormat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $note = null;

    #[ORM\ManyToMany(targetEntity: Masterclass::class, inversedBy: 'refFormats')]
    private Collection $masterclass;

    public function __construct()
    {
        $this->masterclass = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): static
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return Collection<int, Masterclass>
     */
    public function getMasterclass(): Collection
    {
        return $this->masterclass;
    }

    public function addMasterclass(Masterclass $masterclass): static
    {
        if (!$this->masterclass->contains($masterclass)) {
            $this->masterclass->add($masterclass);
        }

        return $this;
    }

    public function removeMasterclass(Masterclass $masterclass): static
    {
        $this->masterclass->removeElement($masterclass);

        return $this;
    }
}
