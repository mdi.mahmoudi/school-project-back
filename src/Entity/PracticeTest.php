<?php

namespace App\Entity;

use App\Repository\PracticeTestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PracticeTestRepository::class)]
class PracticeTest
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["masterclass"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["masterclass"])]
    private ?string $question = null;

    #[ORM\Column(type: Types::ARRAY)]
    #[Groups(["masterclass"])]
    private array $answers = [];

    #[ORM\Column(length: 255)]
    #[Groups(["masterclass"])]
    private ?string $rightAnswer = null;

    #[ORM\ManyToOne(inversedBy: 'practiceTests')]
    private ?Masterclass $masterclass = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): static
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswers(): array
    {
        return $this->answers;
    }

    public function setAnswers(array $answers): static
    {
        $this->answers = $answers;

        return $this;
    }

    public function getRightAnswer(): ?string
    {
        return $this->rightAnswer;
    }

    public function setRightAnswer(string $rightAnswer): static
    {
        $this->rightAnswer = $rightAnswer;

        return $this;
    }

    public function getMasterclass(): ?Masterclass
    {
        return $this->masterclass;
    }

    public function setMasterclass(?Masterclass $masterclass): static
    {
        $this->masterclass = $masterclass;

        return $this;
    }
}
