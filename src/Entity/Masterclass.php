<?php

namespace App\Entity;

use App\Repository\MasterclassRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MasterclassRepository::class)]
class Masterclass
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['lesson', 'masterclass'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['lesson', 'masterclass'])]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    #[Groups(['lesson', 'masterclass'])]
    private ?string $video = null;

    #[ORM\ManyToMany(targetEntity: RefComposer::class, mappedBy: 'masterclass')]
    #[Groups(['lesson', 'masterclass'])]
    private Collection $refComposers;

    #[ORM\ManyToOne(inversedBy: 'masterclasses')]
    #[Groups(['masterclass'])]
    private ?Lesson $lesson = null;

    #[ORM\OneToMany(mappedBy: 'masterclass', targetEntity: PracticeTest::class)]
    #[Groups(['masterclass'])]
    private Collection $practiceTests;

    #[ORM\Column(length: 255)]
    private ?string $image = null;

    public function __construct()
    {
        $this->refComposers = new ArrayCollection();
        $this->practiceTests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): static
    {
        $this->video = $video;

        return $this;
    }

    /**
     * @return Collection<int, RefComposer>
     */
    public function getRefComposers(): Collection
    {
        return $this->refComposers;
    }

    public function addRefComposer(RefComposer $refComposer): static
    {
        if (!$this->refComposers->contains($refComposer)) {
            $this->refComposers->add($refComposer);
            $refComposer->addMasterclass($this);
        }

        return $this;
    }

    public function removeRefComposer(RefComposer $refComposer): static
    {
        if ($this->refComposers->removeElement($refComposer)) {
            $refComposer->removeMasterclass($this);
        }

        return $this;
    }

    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(?Lesson $lesson): static
    {
        $this->lesson = $lesson;

        return $this;
    }

    /**
     * @return Collection<int, PracticeTest>
     */
    public function getPracticeTests(): Collection
    {
        return $this->practiceTests;
    }

    public function addPracticeTest(PracticeTest $practiceTest): static
    {
        if (!$this->practiceTests->contains($practiceTest)) {
            $this->practiceTests->add($practiceTest);
            $practiceTest->setMasterclass($this);
        }

        return $this;
    }

    public function removePracticeTest(PracticeTest $practiceTest): static
    {
        if ($this->practiceTests->removeElement($practiceTest)) {
            // set the owning side to null (unless already changed)
            if ($practiceTest->getMasterclass() === $this) {
                $practiceTest->setMasterclass(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): static
    {
        $this->image = $image;

        return $this;
    }
}
