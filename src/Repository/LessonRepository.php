<?php

namespace App\Repository;

use App\Entity\Lesson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Lesson>
 *
 * @method Lesson|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lesson|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lesson[]    findAll()
 * @method Lesson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lesson::class);
    }

    public function save(Lesson $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Lesson $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getSortedLessons(): array
    {
        return $this->createQueryBuilder('l')
            ->addSelect('u')
            ->leftJoin('l.user', 'u')
            ->orderBy('l.date', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getMasterclassesInLesson(int $lessonId): array
    {
        return $this->createQueryBuilder('l')
            ->select(['m.id','m.title'])
            ->leftJoin('l.masterclasses', 'm')
            ->where('l.id = :lessonId')
            ->setParameter('lessonId', $lessonId)
            ->getQuery()
            ->getArrayResult();
    }
}
