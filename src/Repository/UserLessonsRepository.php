<?php

namespace App\Repository;

use App\Entity\UserLessons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserLessons>
 *
 * @method UserLessons|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLessons|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLessons[]    findAll()
 * @method UserLessons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLessonsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLessons::class);
    }

    public function save(UserLessons $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(UserLessons $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getUserLessons(int $userId, int $lessonId): array
    {
        return $this->createQueryBuilder('ul')
            ->select(['ul.id', 'ul.state', 'm.id as masterclassId', 'm.title'])
            ->leftJoin('ul.user', 'u')
            ->leftJoin('ul.lessons', 'l')
            ->leftJoin('ul.masterclass', 'm')
            ->where('u.id = :userId')
            ->andWhere('l.id = :lessonId')
            ->setParameter('userId', $userId)
            ->setParameter('lessonId', $lessonId)
            ->getQuery()
            ->getArrayResult();
    }

    public function getUserLessonsId(int $userId, int $masterclassId): array
    {
        return $this->createQueryBuilder('ul')
            ->select('ul.id')
            ->leftJoin('ul.user', 'u')
            ->leftJoin('ul.masterclass', 'm')
            ->where('u.id = :userId')
            ->andWhere('m.id = :masterclassId')
            ->setParameter('userId', $userId)
            ->setParameter('masterclassId', $masterclassId)
            ->getQuery()
            ->getArrayResult();
    }
}
