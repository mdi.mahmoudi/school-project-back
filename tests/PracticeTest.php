<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\PracticeTest as PracticeTestEntity;

class PracticeTest extends TestCase
{
    public function testSomething(): void
    {
        $practiceTest = new PracticeTestEntity();
        $practiceTest->setQuestion("Ma question");
        $this->assertTrue($practiceTest->getQuestion() === 'Ma question');
    }
}
