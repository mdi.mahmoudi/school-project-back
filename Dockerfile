FROM  --platform=linux/amd64 debian:stable

RUN apt-get update && apt-get install -y nginx git wget unzip supervisor

RUN apt-get install  -y php-common php-cli php-fpm php-xml php-zip php-pdo php-pdo-mysql

RUN wget https://getcomposer.org/installer

RUN  php installer --install-dir=bin --filename=composer

COPY ./docker/vhost.conf /etc/nginx/conf.d/default.conf
COPY ./docker/nginx.conf /etc/nginx/
COPY ./docker/www.conf /etc/php/8.2/fpm/pool.d

COPY ./docker/supervisor/supervisord.conf /etc/supervisor/
COPY ./docker/supervisor/nginx.conf /etc/supervisor/conf.d/
COPY ./docker/supervisor/php-fpm.conf /etc/supervisor/conf.d/


COPY . /var/www/

WORKDIR /var/www

RUN composer install

CMD ["supervisord" ,"-n"]
